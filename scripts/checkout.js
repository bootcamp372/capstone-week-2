        /*On Change listener for shiptobilling checkbox; disables shipping fields when shiptobilling checkbox is checked*/
        document.getElementById('shiptobilling').onchange = function () {

            document.getElementById('firstNameShipping').disabled = this.checked;
            document.getElementById('lastNameShipping').disabled = this.checked;
            document.getElementById('addressShipping').disabled = this.checked;
            document.getElementById('apartmentShipping').disabled = this.checked;
            document.getElementById('cityShipping').disabled = this.checked;
            document.getElementById('stateShipping').disabled = this.checked;
            document.getElementById('zipShipping').disabled = this.checked;
        };

        /*On click listener for submitCheckout button; shows a confirmation modal if validInputs() function returns true and stops event propogation so modal persists*/
        document.getElementById('submitCheckout').onclick = function (event) {

            const isValid = validInputs();
            if (isValid) {
                const confirmationModal = new bootstrap.Modal(document.getElementById("confirmationModal"), {});
                confirmationModal.show();
                event.preventDefault();
                event.stopPropagation();
            }
        }

        /*validInputs function checks all inputs for validity and returns isValid boolean value*/

        function validInputs() {

            let inputs, index;
            let isValid = true;

            inputs = document.getElementsByTagName('input');

            for (index = 0; index < inputs.length; ++index) {
                var currentInput = inputs[index];
                if (!currentInput.checkValidity()) {
                    isValid = false;
                    return isValid;
                }
            }
            return isValid;
        }