# Capstone Week 2 Project - Let it Brie International Cheese Shop

This is the capstone week 2 project for the Centene Immersion Bootcamp course. It is a website shell for an eCommerce site using HTML, CSS, Bootstrap, and a little bit of Javascript.

## Website Description

The concept for this website is a cheese shop called "Let It Brie" that sells specialty cheeses from around the world. 

The website has 12 html pages, represented by links on the page. All sites are responsive for x-small to x-large screen sizes.

1. Home Page - index.html

Contains the following:
    *navigation bar with links and 2 dropdown links:
        *Navbar > About
            *Location link - links to 1 Campus Martius, Detroit, MI 48226 in Google Maps
            *Contact us - opens email browser
        *International CHeeses
            *Contains links to all cheese product pages
    *footer with links similar to NavBar links
    *intro paragraph and "Shop All" button that links to all_products.html
    *shop cheeses button that links to all_cheeses.html
    *contact us link that opens email browser
    *Bestsellers section, row of 3 Bootstrap cards
    *Cheesemaking - Visiting a Swiss Dairyman video
        *Embedded with iframe
        
<p>
  <img src="/images/Website_Screenshots/home_page_1.PNG"  title="Home Page 1">
</p>
<p>
  <img src="/images/Website_Screenshots/home_page_2.PNG"  title="Home Page 2">
</p>
<p>
  <img src="/images/Website_Screenshots/home_page_bestsellers.PNG"  title="Home Page Bestsellers">
</p>
<p>
  <img src="/images/Website_Screenshots/home_page_video.PNG"  title="Home Page Video">
</p>

About > Contact link for Navigation Bar:

<p>
  <img src="/images/Website_Screenshots/about_contact_page.PNG"  title="About Contact Page">
</p>


2. All Cheeses Page - all_cheeses.html

Contains the following:
    *product cards for all cheeses presented in a Bootstrap grip
    *item quantity buttons on each product card

<p>
  <img src="/images/Website_Screenshots/all_cheese_page.PNG"  title="All Cheese Page">
</p>


3. All Products Page - all_products.html

Contains the following:
    *product cards for all products presented in a Bootstrap grip
    *item quantity and delete buttons on each product card

<p>
  <img src="/images/Website_Screenshots/all_products_page.PNG"  title="All Product Page">
</p>


4. Dutch Cheeses Page - dutch_cheeses.html

Contains the following:
    *product cards for Dutch cheeses presented in a Bootstrap grip
    *item quantity and delete buttons on each product card


<p>
  <img src="/images/Website_Screenshots/dutch_cheese_page.PNG"  title="All Dutch Cheeses Page">
</p>


5. English Cheeses Page - english_cheeses.html

Contains the following:
    *product cards for all English cheeses presented in a Bootstrap grip
    *item quantity and delete buttons on each product card

<p>
  <img src="/images/Website_Screenshots/english_cheese_page.PNG"  title="All English Cheeses Page">
</p>


6. French Cheeses Page - french_cheeses.html

Contains the following:
    *product cards for all French cheeses presented in a Bootstrap grip
    *item quantity and delete buttons on each product card

<p>
  <img src="/images/Website_Screenshots/french_cheese_page.PNG"  title="All French Cheeses Page">
</p>


7. Swiss Cheeses Page - swiss_cheeses.html

Contains the following:
    *product cards for all Swiss cheeses presented in a Bootstrap grip
    *item quantity and delete buttons on each product card

<p>
  <img src="/images/Website_Screenshots/swiss_cheese_page.PNG"  title="All Swiss Cheeses Page">
</p>

8. Gift Sets Page - gift_sets.html

Contains the following:
    *product cards for all Gift Sets presented in a Bootstrap grip
    *item quantity and delete buttons on each product card

<p>
  <img src="/images/Website_Screenshots/gift_sets_page.PNG"  title="All Gift Sets Page">
</p>

9. Login Page - login.html

Contains the following:
    *login form presented on a card
    *tab links for login and registration pages
    *login form with regex validation for the following fields:
        *Username
        *Password
    *links to Google, Facebook, twitter
    *Remember Me checkbox
    *Forgot Password link

<p>
  <img src="/images/Website_Screenshots/login_page.PNG"  title="Login Page">
</p>

10. Registration Page - registration.html

Contains the following:
    *registration form presented on a card
    *tab links for login and registration pages
    *registration form with regex validation for the following fields:
        *First Name
        *Last Name
        *Email
        *Phone #
        *Username
        *Password
        *Confirm Password

<p>
  <img src="/images/Website_Screenshots/registration_page.PNG"  title="Registration Page 1">
</p>

<p>
  <img src="/images/Website_Screenshots/registration_page_2.PNG"  title="Registration Page 2">
</p>

11. Shopping Cart Page - shopping_cart.html

Contains the following:
    *4 cards displayed by Bootstrap Grid
        *Cart
        *Expected Shipping Delivery
        *Credit Card Acceptance Info
        *Order Summary - contains unordered list of items and totals 

<p>
  <img src="/images/Website_Screenshots/shopping_cart_page.PNG"  title="Shopping Cart Page">
</p>

12. Checkout Page - checkout.html
    *2 cards displayed by Bootstrap Grid
        *Payment Details - checkout form with credit card, shipping, and billing info. 
            Regex validation for the following fields:
            *Card Number
            *Cardholder's Name
            *Expiration
            *CVV
            *First Name
            *Last Name
            *City
            *Zip
            Contains a link to the shopping cart page and a submit button on Payment Details card

        *Your Order - card containing a table of items from Shopping Cart Page
    *Confirmation Modal upon successful form validation
    *checkout.js
        *contains listener for shiptobilling checkbox that disables shipping fields
        *contains validation fuction for form: validInputs()
        *contains onclick listener for submit button that displays order confirmation modal

<p>
  <img src="/images/Website_Screenshots/checkout_page_1.PNG"  title="Checkout Page 1">
</p>

<p>
  <img src="/images/Website_Screenshots/checkout_page_2.PNG"  title="Checkout Page 2">
</p>

<p>
  <img src="/images/Website_Screenshots/checkout_page_confirmation_modal.PNG"  title="Checkout Confirmation Modal">
</p>

## Files and Directories

File structure for the project is as follows:

```
capstone-week-2
│   README.md
│   all_cheeses.html 
|   all_products.html
|   dutch_cheeses.html
|   english_cheeses.html
|   french_cheeses.html
|   swiss_cheeses.html
|   gift_sets.html
|   login.html  
│   registration.html
|   shopping_cart.html
|   checkout.html
|   README.md
|
└───assets
│   │   IMFellDWPicaSC-Regular.ttf  
└───images
│   |   cheese_banner.jpg
│   |   cheese-1.png  
│   |   cheese-2.png     
│   |   gift_basket.jpg
|   |   icons8-cheese-100.png    
│   |
│   |───Dutch_Cheese
│   |   Edam.jpg
│   |   Gouda.jpg
│   |   Kanterkaas.jpg
│   |   Beemster.jpg
│   |   Leyden.jpg
│   |   Gouda.jpg
│   |
│   |───French_Cheese
│   |   Bleu_auvergne.jpg
│   |   Brie.jpg
│   |   Camemebert.jpg
│   |   Muenster.jpg
│   |   Roquefort.jpg
│   |
│   |───English_Cheese
│   |   Sage_Derby.jpg
│   |   Red_Leicester.jpg
│   |   Gloucester.jpg
│   |   Blue_Stilton.jpg
│   |   Chesire_Cheese.jpg
│   |
│   |───Swiss_Cheese
│   |   Appenzeller.jpg
│   |   Bleuchatel.jpg
│   |   Gruyere.jpg
│   |   Schabziger.jpg
│   |
│   |───Flags
│   |   British_Flag.png
│   |   Dutch_Flag.png
│   |   French_Flag.png
│   |   Swiss_Flag.png
│   |
│   |───Website_Screenshots
│   |   contains screenshots of the 11 html pages and modal listed above in "Website Description" section
|───css
│   |   styles.css
│
|───scripts
|   |   checkout.js

```

## Additional Resources Used

This site uses Bootstrap 5.3.0, which can be downloaded here: https://getbootstrap.com/
This site uses Fontawesome 4.7.0 for icons, which can be accessed here: https://fontawesome.com/v4/icons/
This site is using the following font from Google fonts: Using following font from google fonts: https://fonts.google.com/specimen/IM+Fell+DW+Pica

## Setup

To get started with the project, clone the repository from the following Gitlab link: https://gitlab.com/bootcamp372/capstone-week-2

## Design Notes

Checkout Page features a confirmation modal that is shown after validation. JS code is below:

```
        /*On Change listener for shiptobilling checkbox; disables shipping fields when shiptobilling checkbox is checked*/
        document.getElementById('shiptobilling').onchange = function () {

            document.getElementById('firstNameShipping').disabled = this.checked;
            document.getElementById('lastNameShipping').disabled = this.checked;
            document.getElementById('addressShipping').disabled = this.checked;
            document.getElementById('apartmentShipping').disabled = this.checked;
            document.getElementById('cityShipping').disabled = this.checked;
            document.getElementById('stateShipping').disabled = this.checked;
            document.getElementById('zipShipping').disabled = this.checked;
        };

        /*On click listener for submitCheckout button; shows a confirmation modal if validInputs() function returns true and stops event propogation so modal persists*/
        document.getElementById('submitCheckout').onclick = function (event) {

            const isValid = validInputs();
            if (isValid) {
                const confirmationModal = new bootstrap.Modal(document.getElementById("confirmationModal"), {});
                confirmationModal.show();
                event.preventDefault();
                event.stopPropagation();
            }
        }

        /*validInputs function checks all inputs for validity and returns isValid boolean value*/

        function validInputs() {

            let inputs, index;
            let isValid = true;

            inputs = document.getElementsByTagName('input');

            for (index = 0; index < inputs.length; ++index) {
                var currentInput = inputs[index];
                if (!currentInput.checkValidity()) {
                    isValid = false;
                    return isValid;
                }
            }
            return isValid;
        }
```


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

Gitlab link: https://gitlab.com/bootcamp372/capstone-week-2